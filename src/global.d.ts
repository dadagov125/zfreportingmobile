import {Action, Middleware} from "redux";
import {AxiosInstance, AxiosRequestConfig, AxiosResponse} from "axios";


declare module "redux-axios-middleware" {
  export default function (client: AxiosInstance): Middleware;
}


//actions
declare global {

  type IAction = {
    payload: AxiosResponse<any> | { request: AxiosRequestConfig }
  } & Action<string>

  type RootState = {
    readonly sale: SaleState,
    readonly account:AccountState
  }

  type SaleState = {
    readonly items: Sale[],
    readonly isLoading: boolean
  }

  type AccountState={
    readonly user:User;
    readonly isAuth:boolean;
    readonly isLoading:boolean;
  }


  type Sale = {

    id: string

    date: string

    price: number

    user: string

    product: string

    count: number
  }


  type User = {

    id: string;

    companyName: string

    firstName: string

    lastName: string

    patronymic: string

    role: string

    userName: string

    email: string

    phoneNumber: string

  }

}












