export const GET_SALES = 'GET_SALES';
export const GET_SALES_SUCCESS = 'GET_SALES_SUCCESS';
export const GET_SALES_FAIL = 'GET_SALES_FAIL';

export const GET_SALE = 'GET_SALE';
export const GET_SALE_SUCCESS = 'GET_SALE_SUCCESS';
export const GET_SALE_FAIL = 'GET_SALE_FAIL';

export const SET_SALES='SET_SALES';

export const CREATE_SALE='CREATE_SALE';
export const CREATE_SALE_SUCCESS='CREATE_SALE_SUCCESS';
export const CREATE_SALE_FAIL='CREATE_SALE_FAIL';

export const UPDATE_SALE='UPDATE_SALE';
export const UPDATE_SALE_SUCCESS='UPDATE_SALE_SUCCESS';
export const UPDATE_SALE_FAIL='UPDATE_SALE_FAIL';

export const DELETE_SALE='DELETE_SALE';
export const DELETE_SALE_SUCCESS='DELETE_SALE_SUCCESS';
export const DELETE_SALE_FAIL='DELETE_SALE_FAIL';




export const getSales = (): IAction => ({
  type: GET_SALES,
  payload: {
    request: {
      url: '/sale',
      method: 'get'
    }
  }
});

export const getSale=(id:string):IAction=>({
  type: GET_SALE,
  payload: {
    request: {
      url: `/sale/${id}`,
      method: 'get'
    }
  }
});

export const setSales=(...sale:Sale[]):IAction=>({
  type:SET_SALES,
  payload:{
    data:sale
  }
})

export const createSale=(sale:Sale):IAction=>({
  type:CREATE_SALE,
  payload:{
    request:{
      url:`/sale`,
      method:'post',
      data:sale
    }
  }

});

export const updateSale=(sale:Sale):IAction=>({

  type:UPDATE_SALE,
  payload:{
    request:{
      url:`/sale`,
      method:'put',
      data:sale
    }
  }

});

export const deleteSale=(sale:Sale):IAction=>({
  type:DELETE_SALE,
  payload:{
    request:{
      url:`/sale/${sale.id}`,
      method:'delete'
    }
  }

})