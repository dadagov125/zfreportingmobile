export const CHECK_AUTH = 'CHECK_AUTH';
export const CHECK_AUTH_SUCCESS = 'CHECK_AUTH_SUCCESS';
export const CHECK_AUTH_FAIL = 'CHECK_AUTH_FAIL';

export const LOGIN = 'LOGIN';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';

export const LOGOUT='LOGOUT';
export const LOGOUT_SUCCESS='LOGOUT_SUCCESS';
export const LOGOUT_FAIL='LOGOUT_FAIL';


export const checkAuth = (): IAction => ({
  type: CHECK_AUTH,
  payload: {
    request: {
      url: '/account/me',
      method: 'get'
    }
  }
});

export const login = (login: string, password: string, rememberMe: boolean = true): IAction => ({
  type: LOGIN,
  payload: {
    request: {
      url: '/account/login',
      method: 'post',
      data:{
        email:login,
        password,
        rememberMe
      }

    }
  }
});

export const logout=():IAction=>({
  type:LOGOUT,
  payload:{
    request:{
      url:'/account/logout',
      method: 'post',
    }
  }
})