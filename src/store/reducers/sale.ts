import {
  CREATE_SALE,
  CREATE_SALE_FAIL,
  CREATE_SALE_SUCCESS, DELETE_SALE, DELETE_SALE_FAIL, DELETE_SALE_SUCCESS,
  GET_SALE,
  GET_SALE_FAIL,
  GET_SALE_SUCCESS,
  GET_SALES,
  GET_SALES_FAIL,
  GET_SALES_SUCCESS,
  SET_SALES,
  UPDATE_SALE,
  UPDATE_SALE_FAIL,
  UPDATE_SALE_SUCCESS
} from "../actions/sale";
import {AxiosResponse} from "axios";
import {setItems} from "./index";


export const initSaleState: SaleState = {
  items: [],
  isLoading: false

};


export function saleReducer(state: SaleState = initSaleState, action: IAction) {


  switch (action.type) {
    case DELETE_SALE:
    case UPDATE_SALE:
    case CREATE_SALE:
    case GET_SALE:
    case GET_SALES:
      return {
        ...state,
        isLoading: true
      };

    case DELETE_SALE_FAIL:
    case UPDATE_SALE_FAIL:
    case CREATE_SALE_FAIL:
    case GET_SALE_FAIL:
    case GET_SALES_FAIL:
      return {
        ...state,
        isLoading: false
      };



    case UPDATE_SALE_SUCCESS:
    case CREATE_SALE_SUCCESS:
    case GET_SALE_SUCCESS:
    case GET_SALES_SUCCESS:
    case SET_SALES: {
      let newState = {...state, isLoading: false} as SaleState;

      const data = (action.payload as AxiosResponse).data;

      if (Array.isArray(data)) {
        setItems(newState, ...data)
      } else {
        setItems(newState, data)
      }

      return newState;
    }

    case DELETE_SALE_SUCCESS:{

      let newState = {...state, isLoading: false} as SaleState;

      const sale = (action.payload as AxiosResponse<Sale>).data;

      let existItem = newState.items.find(s => s.id == sale.id);
      if (existItem) {
        newState.items.splice(newState.items.indexOf(existItem), 1);
      }

      return newState;
    }

    default:
      return state;

  }
}

// function setItems(newState: SaleState, ...sales: Sale[]) {
//
//   sales.forEach((sale, i) => {
//     let existItem = newState.items.find(s => s.id == sale.id);
//     if (existItem) {
//       newState.items.splice(newState.items.indexOf(existItem), 1, sale);
//     } else {
//       newState.items.push(sale)
//     }
//   })
// }
