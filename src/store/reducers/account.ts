

import {
  CHECK_AUTH, CHECK_AUTH_FAIL, CHECK_AUTH_SUCCESS, LOGIN, LOGIN_FAIL, LOGIN_SUCCESS,
  LOGOUT, LOGOUT_FAIL, LOGOUT_SUCCESS
} from "../actions/account";
import {AxiosResponse} from "axios";

export const initAccountState:AccountState={
  user:null,
  isAuth:false,
  isLoading:false
};

export function accountReducer(state:AccountState=initAccountState,action:IAction){

  switch (action.type){
    case LOGOUT:
    case LOGIN:
    case CHECK_AUTH:{
      return {...state, isLoading:true}
    }

    case LOGOUT_FAIL:
    case LOGIN_FAIL:
    case CHECK_AUTH_FAIL:{
      return {...state, isLoading:false}
    }

    case LOGIN_SUCCESS:
    case CHECK_AUTH_SUCCESS:{
    const user=(action.payload as AxiosResponse<User>).data;
    return {...state, isLoading:false, isAuth:true, user:user}
    }

    case LOGOUT_SUCCESS:{
      return {...state, isLoading:false,isAuth:false, user:null}
    }


    default:{
      return state;
    }
  }



}