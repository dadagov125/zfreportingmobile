import {combineReducers} from 'redux';
import {saleReducer} from "./sale";
import {accountReducer} from "./account";


export const reducers = combineReducers<RootState>({
  sale: saleReducer,
  account:accountReducer
});


export function setItems<S, E>(newState: S, ...items: E[]){

  items.forEach((item) => {
    let existItem = newState.items.find(i => i.id == item.id);
    if (existItem) {
      newState.items.splice(newState.items.indexOf(existItem), 1, item);
    } else {
      newState.items.push(item)
    }
  })

}