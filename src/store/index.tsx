import * as React from 'react';
import {applyMiddleware, createStore} from 'redux';
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import {reducers} from './reducers';
import {initSaleState} from "./reducers/sale";
import {initAccountState} from "./reducers/account";

const apiClient = axios.create({
  baseURL: 'http://localhost:5000/api',
  responseType: 'json'
});


const initialState: RootState = {
  sale: initSaleState,
  account:initAccountState
};


export default function () {
  return createStore(
      reducers,
      initialState,
      applyMiddleware(
          axiosMiddleware(apiClient)
      )
  );

}
