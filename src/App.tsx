

import React, {Component} from 'react';
import {Platform} from 'react-native';
import createStore from './store'
import {Provider} from "react-redux";
import Routes from './navigation'


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
  'Double tap R on your keyboard to reload,\n' +
  'Shake or press menu button for dev menu',
});

const store = createStore();

export default class App extends Component {

  render() {
    return (

        <Provider store={store}>
          <Routes></Routes>

        </Provider>
    );
  }
}


