import React, {Component} from 'react';
import {Button, Card, CardItem, Container, Content, Icon, Left, Right, Text} from "native-base";
import {Image, StyleSheet, TouchableOpacity} from 'react-native'
import _ from 'lodash'
import moment from 'moment'
import {Actions} from 'react-native-router-flux';
import {Common, getUser} from "../Common";

function onError(err) {
	console.log(err)
}

let costType;

let user;

export class CostList extends Component {



	static navigationOptions = {
		headerRight:(  <Button icon rounded transparent onPress={() => {
				Actions.costEdit({type: 0, isNew: true, cost: null})
			}}>
				<Icon name='add'/>
			</Button>
		)
	};

	constructor(props) {
		super(props)

		let routeName = this.props.name;

		let type;

		if (routeName == '_costsPlan') {
			type = 0
		}
		if (routeName == '_costsActual') {
			type = 1
		}
		costType = type

		this.state = {
			user: getUser(),
			costs: [],
			type: type
		}


	}


	componentWillMount() {
		// fetch('http://localhost:5000/api/account/me').then(res=>{
		// 	console.log(res)
		// 	if(res.status!=200){
		// 		throw new Error(res.status)
		// 	}
		//
		// }).catch(onError)


		fetch('http://localhost:5000/api/cost?type=' + this.state.type)
			.then((res) => {
				res.json()
					.then(sales => {
						this.setState(state => {
							return Object.assign({}, state, {costs: sales})
						})
					}, onError)
			}, onError)

	}


	getCostListItems() {

		return _.map(_.groupBy(this.state.costs, 'date'), (costs, i) => {
			return (<Card key={i}>
				<CardItem header>
					<Text>{moment(i).format('MMMM YYYY')}</Text>
				</CardItem>
				{this.getCostList(costs)}
			</Card>)


		});
	}

	getCostList(costs) {

		return _.map(costs, c => {
			return (<TouchableOpacity key={c.id} onPress={() => {
				if (getUser()?.role != 'Admin')
				return;
				Actions.costEdit({type: c.type, isNew: false, cost: c, user: this.props.user})

			}

			}>


				<CardItem>
					<Left>
						<Text>Себестоимость</Text>
					</Left>
					<Right>
						<Text>100р</Text>
					</Right>
				</CardItem>

				<CardItem>
					<Left>
						<Text>Коммунальные расходы</Text>
					</Left>
					<Right>
						<Text>100р</Text>
					</Right>
				</CardItem>

				<CardItem>
					<Left>
						<Text>Транспортные расходы</Text>
					</Left>
					<Right>
						<Text>100р</Text>
					</Right>
				</CardItem>

				<CardItem>
					<Left>
						<Text>Аренды</Text>
					</Left>
					<Right>
						<Text>100р</Text>
					</Right>
				</CardItem>

				<CardItem>
					<Left>
						<Text>Оплата труда</Text>
					</Left>
					<Right>
						<Text>100р</Text>
					</Right>
				</CardItem>

				<CardItem>
					<Left>
						<Text>Мелкие затраты</Text>
					</Left>
					<Right>
						<Text>100р</Text>
					</Right>
				</CardItem>

				<CardItem>
					<Left>
						<Text>Маркетинг и продвижение</Text>
					</Left>
					<Right>
						<Text>100р</Text>
					</Right>
				</CardItem>

				<CardItem>
					<Left>
						<Text>Аудит и бухучет</Text>
					</Left>
					<Right>
						<Text>100р</Text>
					</Right>
				</CardItem>

				<CardItem>
					<Left>
						<Text>Страхование</Text>
					</Left>
					<Right>
						<Text>100р</Text>
					</Right>
				</CardItem>


			</TouchableOpacity>)
		})
	}


	render() {
		console.log(this.state.costs)
		return (
			<Container>
				<Content>


					{this.getCostListItems()}


				</Content>

			</Container>


		)
	}

}

//
// <Picker
// 	mode="dropdown"
// 	placeholder="Тип продажи"
// 	// selectedValue={this.state.selected2}
// 	onValueChange={()=>{}}
// >
// 	<Picker.Item label="Прогноз продажи" value="0" />
// 	<Picker.Item label="Фактическая продажа" value="1" />
//
// </Picker>

const styles = StyleSheet.create({
	col: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'flex-start',
		alignItems: 'flex-end',


	}
});