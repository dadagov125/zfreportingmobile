import React, {Component} from 'react';
import {Button, Container, Content, Form, Header, Icon, Input, Item, Text, View} from "native-base";
import {Image} from 'react-native'
import {Actions} from 'react-native-router-flux';
import logo from '../assets/img/zf_logo.png'
import {setUser} from "../Common";
import axios from 'axios'
import {Dispatch} from "redux";
import {getSale, getSales, setSales, deleteSale} from "../store/actions/sale";
import {connect} from 'react-redux';
import {checkAuth, login, logout} from "../store/actions/account";


function onError(err) {
  console.log(err)
}


type OwnProps = {}

type StateFromProps = {
  sales?: any[]

}


type DispatchFromProps = {
  getSales();
  getSale(id:string);
  setSale(...sale:Sale[]);
  deleteSate(sale:Sale);
  checkAuth();
}


class Login extends Component<OwnProps & StateFromProps & Dispatch, any> {


  constructor(props:any) {
    super(props);
    this.state = {
      login: 'admin@mail.ru',
      password: '1234567',
      isLoading: false
    }

  }


  componentWillMount() {

  }


  isAdmin(user) {
    return user.role == "Admin";
  }

  isConsult(user) {
    return user.role == "Consult";
  }

  isClient(user) {
    return user.role == "Client";
  }

  //
  // navigateTo(user) {
  //
  //
  //
  // 	if (this.isAdmin(user)) {
  // 		Actions.drawer({user: user})
  // 		Actions.consultants({user: user})
  // 	}
  // 	if (this.isConsult(user)) {
  // 		Actions.drawer({user: user})
  // 		Actions.clients({user: user})
  // 	}
  // 	if (this.isClient(user)) {
  // 		Actions.consultants({user: user})
  // 	}
  //

  // }

  doLogin() {
    this.props.getSales();

    //this.props.checkAuth()



    return

    this.setState(state => {

      let newState = Object.assign({}, state, {})

      newState.isLoading = true;
      return newState
    })

    let body = {
      Email: this.state.login,
      Password: this.state.password,
      RememberMe: true
    };


    axios.post('http://localhost:5000/api/account/login', body).then(res => {
      if (res.status !== 200) {
        this.setState(state => {
          let newState = Object.assign({}, state, {})
          newState.isLoading = false;
          return newState
        })
      }


      setUser(res.data)
      Actions.drawer({user: res.data})




    }).catch(onError)
  }

  onChange(field, value) {
    this.setState(state => {

      let newState = Object.assign({}, state, {})

      newState[field] = value;
      return newState
    })
  }


  render() {

    return (<Container>
      <Header transparent/>
      <Content padder>
        <View>
          <Image source={logo}/>
        </View>
        <Form>

          <Item fixedLabel>
            <Icon type="MaterialCommunityIcons" name="email"/>
            <Input value={this.state.login} onChangeText={this.onChange.bind(this, 'login')} placeholder={'Логин'}/>
          </Item>


          <Item fixedLabel last>
            <Icon type="MaterialCommunityIcons" name="lock"/>
            <Input secureTextEntry value={this.state.password} onChangeText={this.onChange.bind(this, 'password')}
                   placeholder={'Пароль'}/>
          </Item>
          <Button
              block primary
              onPress={this.doLogin.bind(this)}>
            <Text>Войти</Text>
          </Button>
        </Form>

      </Content>
    </Container>)
  }
}

const mapStateToProps = (state: RootState): StateFromProps => {

  return {
  sales: state.sale.items
}};

const mapDispatchToProps = (dispatch: Dispatch<RootState>): DispatchFromProps => ({
  getSales() {
    dispatch(getSales())
  },
  getSale(id:string){
    dispatch(getSale(id));
  },

  setSale(...sale:Sale[]){
   dispatch(setSales(...sale))
  },
  deleteSale(sale:Sale){
    dispatch(deleteSale(sale))
  },
  checkAuth(){
    dispatch(logout()).then(res=>{


    })
  }

});

export default connect<StateFromProps, DispatchFromProps, OwnProps>(mapStateToProps, mapDispatchToProps)(Login);