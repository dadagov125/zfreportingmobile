import React, {Component} from 'react';
import {Body, Button, Container, Content, Footer, Form, Input, Item, Label, Text, DatePicker} from "native-base";
import {Image, StyleSheet} from 'react-native'
import moment from 'moment'
import {getUser} from "../Common";
import {Actions} from 'react-native-router-flux';
function onError(err) {
	console.log(err)
}

let costType;

let user;

export class CostEdit extends Component {
	static navigationOptions = {
		//title:costType==0?'Прогноз расхода':'Расход'
	};

	constructor(props) {
		super(props)

		console.log(props)

		costType = props.type


		user= getUser();

		this.state = {
			isNew: props.isNew,
			cost: props.isNew ? {

				costType:user && user.role=='Admin'?0:1,

				costPrice:'',

				communalCost:'',

				transportCost:'',

				rentalCost:'',

				laborCost:'',

				minorCost:'',

				marketingCost:'',

				accountingCost:'',

				insuranceCost:''




			} : props.cost
		}
	}

	onChange(field, value) {
		console.log(field, value)


		this.setState(state => {
			let newState = Object.assign({}, state, {})
			newState.cost[field] = value
			return newState
		})


	}


	save(){

		let data=Object.assign({},this.state.cost,{user:user && user.role=='Client'?user:null})

		let body = JSON.stringify(data);

		let method=this.state.isNew?'post':'put'

		fetch('http://localhost:5000/api/cost', {
			method: method,
			body: body,
			headers: new Headers({'content-type': 'application/json'})
		}).then(res => {

			if (!res.ok) {
				return console.log(res.status)
			}
			res.json().then(data => {

				console.log(data)
				Actions.drawer({user: user})

			})

		}).catch(onError)

	}

	render() {
		return (
			<Container>

				<Content>

					<Form>

						<Item>
							<Label>Дата</Label>
							<DatePicker
								defaultDate={new Date(2017, 12, 30,12,0,0)}

								locale={"ru"}
								formatChosenDate={(date)=>{
									return moment(date).format('MMMM YYYY')
								}}
								modalTransparent={false}
								animationType={"slide"}
								androidMode={"default"}
								placeHolderText=""
								textStyle={{ color: "black" }}
								placeHolderTextStyle={{ color: "black" }}
								onDateChange={this.onChange.bind(this, 'date')}
							/>
						</Item>

						<Item floatingLabel>
							<Label>Расход на себестоимость</Label>
							<Input value={this.state.cost.costPrice.toString()} onChangeText={this.onChange.bind(this, 'costPrice')}
							       placeholder=""/>
						</Item>

						<Item floatingLabel>
							<Label>Коммунальные расходы</Label>
							<Input value={this.state.cost.communalCost.toString()} onChangeText={this.onChange.bind(this, 'communalCost')} placeholder=""/>
						</Item>

						<Item floatingLabel>
							<Label>Транспортные расходы</Label>
							<Input value={this.state.cost.transportCost.toString()} onChangeText={this.onChange.bind(this, 'transportCost')} placeholder=""/>
						</Item>

						<Item floatingLabel>
							<Label>Аренда</Label>
							<Input value={this.state.cost.rentalCost.toString()} onChangeText={this.onChange.bind(this, 'rentalCost')}  placeholder=""/>
						</Item>

						<Item floatingLabel>
							<Label>Оплата труда</Label>
							<Input value={this.state.cost.laborCost.toString()} onChangeText={this.onChange.bind(this, 'laborCost')}  placeholder=""/>
						</Item>

						<Item floatingLabel>
							<Label>Мелкие затраты</Label>
							<Input value={this.state.cost.minorCost.toString()} onChangeText={this.onChange.bind(this, 'minorCost')}  placeholder=""/>
						</Item>

						<Item floatingLabel>
							<Label>Маркетинг и продвижение</Label>
							<Input value={this.state.cost.marketingCost.toString()} onChangeText={this.onChange.bind(this, 'marketingCost')}  placeholder=""/>
						</Item>

						<Item floatingLabel>
							<Label>Аудит и бухучет</Label>
							<Input value={this.state.cost.accountingCost.toString()} onChangeText={this.onChange.bind(this, 'accountingCost')}  placeholder=""/>
						</Item>

						<Item floatingLabel>
							<Label>Страхование</Label>
							<Input value={this.state.cost.insuranceCost.toString()} onChangeText={this.onChange.bind(this, 'insuranceCost')}  placeholder=""/>
						</Item>

					</Form>
				</Content>
				<Footer>
					<Body>
					<Button onPress={this.save.bind(this)} full success style={styles.saveBtn}>
						<Text>
							Сохранить
						</Text>
					</Button>

					</Body>


				</Footer>
			</Container>


		)
	}

}


const styles = StyleSheet.create({
	saveBtn: {}

})