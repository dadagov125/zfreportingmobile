import React, {Component} from 'react';
import {Container, Content, Header, List, ListItem, Text, Button, Icon, Left, Right, Body, Footer,View, Item} from "native-base";
import {SafeAreaView, StyleSheet} from 'react-native'
import { Actions } from 'react-native-router-flux';

export class Menu extends Component {

	constructor(props){
		super(props)

		this.state={
			user:props.navigation.getParam('user', null)
		}

		console.log(this.state.user)


		setTimeout(()=>{
			console.log(this.state.user)

			if(this.isAdmin()){
				Actions.consultants({user:this.state.user})
			}
			if(this.isConsult()){
				Actions.clients({user:this.state.user})
			}
			if(this.isClient()){
				Actions.costsPlan({user:this.state.user})
			}



		},100)
	}


	isAdmin(){
		return this.state.user.role=="Admin";
	}

	isConsult(){
		return this.state.user.role=="Consult";
	}

	isClient(){
		return this.state.user.role=="Client";
	}

	getMenuItems(){

		if(this.isAdmin()){

			return (<View>

				<ListItem icon onPress={()=>{Actions.consultants()}}>
					<Left>
						<Button style={{ backgroundColor: "#6463ff" }}>
							<Icon type="AntDesign" name="customerservice" />
						</Button>
					</Left>
					<Body>
					<Text>Консультанты</Text>
					</Body>
					<Right>
						<Icon type="SimpleLineIcons" name="arrow-right" />
					</Right>
				</ListItem>


				<ListItem icon onPress={()=>{Actions.clients()}}>
					<Left>
						<Button style={{ backgroundColor: "#4d141d" }}>
							<Icon type="Foundation" name="torso-business" />
						</Button>
					</Left>
					<Body>
					<Text>Клиенты</Text>
					</Body>
					<Right>
						<Icon type="SimpleLineIcons" name="arrow-right" />
					</Right>
				</ListItem>


			</View>)

		}
		if(this.isConsult()){
			return (<View>

					<ListItem icon onPress={()=>{Actions.clients()}}>
						<Left>
							<Button style={{ backgroundColor: "#4d141d" }}>
								<Icon type="Foundation" name="torso-business" />
							</Button>
						</Left>
						<Body>
						<Text>Клиенты</Text>
						</Body>
						<Right>
							<Icon type="SimpleLineIcons" name="arrow-right" />
						</Right>
					</ListItem>

				</View>
			)

		}

		if(this.isClient()){

			return (<View>


				<ListItem icon onPress={()=>{Actions.costsPlan()}}>
					<Left>
						<Button style={{ backgroundColor: "#615945" }}>
							<Icon type="AntDesign" name="piechart" />
						</Button>
					</Left>
					<Body>
					<Text>Прогноз расходов</Text>
					</Body>
					<Right>
						<Icon type="SimpleLineIcons" name="arrow-right" />
					</Right>
				</ListItem>

				<ListItem icon onPress={()=>{Actions.salesPlan()}}>
					<Left>
						<Button style={{ backgroundColor: "#615945" }}>
							<Icon type="Foundation" name="burst-sale" />
						</Button>
					</Left>
					<Body>
					<Text>План продаж</Text>
					</Body>
					<Right>
						<Icon type="SimpleLineIcons" name="arrow-right" />
					</Right>
				</ListItem>

				<ListItem icon onPress={()=>{Actions.costsActual()}}>
					<Left>
						<Button style={{ backgroundColor: "#ffa054" }}>
							<Icon type="AntDesign" name="piechart" />
						</Button>
					</Left>
					<Body>
					<Text>Расходы</Text>
					</Body>
					<Right>
						<Icon type="SimpleLineIcons" name="arrow-right" />
					</Right>
				</ListItem>

				<ListItem icon onPress={()=>{Actions.salesActual()}}>
					<Left>
						<Button style={{ backgroundColor: "#ffa054" }}>
							<Icon type="Foundation" name="burst-sale" />
						</Button>
					</Left>
					<Body>
					<Text>Продажи</Text>
					</Body>
					<Right>
						<Icon type="SimpleLineIcons" name="arrow-right" />
					</Right>
				</ListItem>

				<ListItem icon onPress={()=>{Actions.products()}}>
					<Left>
						<Button style={{ backgroundColor: "#a69dff" }}>
							<Icon type="FontAwesome" name="product-hunt" />
						</Button>
					</Left>
					<Body>
					<Text>Продукты</Text>
					</Body>
					<Right>
						<Icon type="SimpleLineIcons" name="arrow-right" />
					</Right>
				</ListItem>


			</View>)



		}




	}



	render() {
		return (<Container>
			<Header/>
			<Content>
				<List >


{this.getMenuItems()}

					<ListItem icon onPress={()=>{Actions.charts()}}>
						<Left>
							<Button style={{ backgroundColor: "#a69dff" }}>
								<Icon type="FontAwesome" name="bar-chart" />
							</Button>
						</Left>
						<Body>
						<Text>Диаграмма</Text>
						</Body>
						<Right>
							<Icon type="SimpleLineIcons" name="arrow-right" />
						</Right>
					</ListItem>


				</List>

			</Content>

			<Footer>
				<Content>
					<ListItem icon onPress={()=>{Actions.login()}}>
						<Left>
							{/*<Button style={{ backgroundColor: "#737476" }}>*/}
								<Icon type="MaterialIcons" name="exit-to-app" />
							{/*</Button>*/}
						</Left>
						<Body>
						<Text>Выход</Text>
						</Body>
						<Right>
							{/*<Icon type="SimpleLineIcons" name="arrow-right" />*/}
						</Right>
					</ListItem>
				</Content>

			</Footer>
		</Container>)

	}
}

