import React, {Component} from 'react';
import {Body, Button, Card, CardItem, Container, Icon, Left, List, ListItem, Right, Text,Content} from "native-base";
import {Image, StyleSheet} from 'react-native'
import _ from 'lodash'
import { Actions } from 'react-native-router-flux';

function onError(err) {
	console.log(err)
}

export class ClientList extends Component{
	constructor(props){
		super(props)

		this.state={
				users:[]
		}

	}

	componentWillMount() {
		fetch('http://localhost:5000/api/user?role=Client')
			.then((res) => {
				res.json()
					.then(users => {
						console.log(users)
						this.setState(state => {
							return Object.assign({}, state, {users: users})
						})
					}, onError)
			}, onError)
	}


	showClient(c) {
		Actions.client({client:c})
	}

	getClientList() {
		return _.map(this.state.users, c => {

			return (<ListItem key={c.user.id} onPress={this.showClient.bind(this, c.user)}>
				<Text>
					{c.user.firstName}
				</Text>
				<Body>
				<Text>
					{c.user.lastName}
				</Text>
				</Body>
				<Text>
					{c.user.companyName}
				</Text>
			</ListItem>)



		})

	}



	render(){

		return (<Container>
				<Content>
					<List>
						{this.getClientList()}


					</List>
				</Content>
			</Container>

		)

	}





}