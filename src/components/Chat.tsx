import React from 'react';
import { GiftedChat } from 'react-native-gifted-chat'
import logo from '../assets/img/zf_logo.png'

export class Chat extends React.Component {
	state = {
		messages: [],
	}

	componentWillMount() {
		this.setState({
			messages: [
				{
					_id: '1',
					text: 'Ассаламу Алайкум!',
					createdAt: new Date(),
					user: {
						_id: '2',
						name: 'React Native',
						avatar: logo,
					},
				},
			],
		})
	}

	onSend(messages = []) {
		this.setState(previousState => ({
			messages: GiftedChat.append(previousState.messages, messages),
		}))
	}

	render() {
		return (
			<GiftedChat
				messages={this.state.messages}
				onSend={messages => this.onSend(messages)}
				user={{
					_id: 1,
				}}
			/>
		)
	}
}