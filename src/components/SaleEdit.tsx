import React, {Component} from 'react';
import {
	Body, Container, Content, Header, Left, List, ListItem, Form, Input, Item, Label, Button, Footer, Right, Text,
	DatePicker
} from "native-base";
import {Image, StyleSheet} from 'react-native'
import moment from 'moment'
import {Actions} from 'react-native-router-flux';
import {getUser} from "../Common";

function onError(err) {
	console.log(err)
}

let user;

export class SaleEdit extends Component {

	constructor(props){
		super(props)

		user=getUser();
		this.state={
			sale:{
				product:{
					name:''
				},
				count:0,
				price:0
			}
		}

	}

	save(){



		let body = JSON.stringify(this.state.sale);

		let method='post'

		fetch('http://localhost:5000/api/sale', {
			method: method,
			body: body,
			headers: new Headers({'content-type': 'application/json'})
		}).then(res => {

			if (!res.ok) {
				return console.log(res.status)
			}
			res.json().then(data => {

				console.log(data)
				Actions.drawer({user: user})

			})

		}).catch(onError)

	}


	onChange(field, value) {
		this.setState(state => {
			let newState = Object.assign({}, state, {})
			if(field=='name'){
				newState.sale.product.name = value
			}else {
				newState.sale[field] = value
			}

			return newState
		})


	}

	render() {
		return (
			<Container>

				<Content>
					{/*<Header transparent>*/}
					{/*</Header>*/}
					<Form>
						<Item>
							<Label>Дата</Label>
							<DatePicker
								defaultDate={new Date(2017, 12, 30,12,0,0)}

								locale={"ru"}
								formatChosenDate={(date)=>{
									return moment(date).format('MMMM YYYY')
								}}
								modalTransparent={false}
								animationType={"slide"}
								androidMode={"default"}
								placeHolderText=""
								textStyle={{ color: "black" }}
								placeHolderTextStyle={{ color: "black" }}
								onDateChange={this.onChange.bind(this, 'date')}
							/>
						</Item>

						<Item floatingLabel>
							<Label>Продукт</Label>
							<Input value={this.state.sale.product.name} onChangeText={this.onChange.bind(this, 'name')}
							       placeholder=""/>
						</Item>


						<Item floatingLabel>
							<Label>Цена</Label>
							<Input value={this.state.sale.price.toString()} onChangeText={this.onChange.bind(this, 'price')}
							       placeholder=""/>
						</Item>

						<Item floatingLabel>
							<Label>Количество</Label>
							<Input value={this.state.sale.count.toString()} onChangeText={this.onChange.bind(this, 'count')}
							       placeholder=""/>
						</Item>

					</Form>


				</Content>
				<Footer>
					<Body>
						<Button onPress={this.save.bind(this)} full success style={styles.saveBtn}>
							<Text>
								Сохранить
							</Text>
						</Button>

					</Body>


				</Footer>
			</Container>


		)
	}

}


const styles=StyleSheet.create({
	saveBtn:{

	}

})