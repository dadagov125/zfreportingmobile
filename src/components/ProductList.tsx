import React, {Component} from 'react';
import {Container, Content, Header, Left, List, ListItem, Text} from "native-base";
import {Image, StyleSheet} from 'react-native'
import _ from 'lodash'

function onError(err) {
	console.log(err)
}


export class ProductList extends Component {
	constructor(props) {
		super(props)
		this.state = {
			produts: []
		}
	}

	componentWillMount() {
		fetch('http://localhost:5000/api/product')
			.then((res) => {
				res.json()
					.then(prods => {
						this.setState(state => {
							return Object.assign({}, state, {products: prods})
						})
					}, onError)
			}, onError)
	}

	getListItems() {
		return _.map(this.state.products, p => {
			return (
				<ListItem key={p.id}>
					<Left>
						<Text>{p.name}</Text>
					</Left>
					<Text>{p.user.companyName}</Text>
				</ListItem>
			)
		})
	}


	render() {
		return (
			<Container>
				<Content>
					{/*<Header transparent>*/}
					{/*</Header>*/}
					<List>
						{this.getListItems()}
					</List>
				</Content>
			</Container>


		)
	}

}