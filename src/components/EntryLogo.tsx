import React, {Component} from 'react';
import {Button, Container, Content, Fab, Form, Header, Icon, Input, Item, Text, View} from "native-base";
import {Image, StyleSheet} from 'react-native'
import {Actions} from 'react-native-router-flux';
import logo from '../assets/img/zf_logo.png'


function onError(err) {
	console.log(err)
}


export class EntryLogo extends Component {


	constructor(props){
		super(props)


	}


	componentWillMount(){
		fetch('http://localhost:5000/api/account/me').then(res=>{

			//if(res.status!=200){
				return Actions.login()
			//}
			//Actions.drawer()
		}).catch(onError)

	}




	render() {
		return (<Container>
			<Header transparent/>
			<Content padder>


				<View style={styles.logo}>
					<Image source={logo}/>
				</View>
			</Content>
		</Container>)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'stretch'
	},
	form: {
		paddingTop: 0
	},
	loginBtn: {
		marginTop: 20
	},
	logo: {
		// backgroundColor:'red',
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		height: 300
	}
});