import React, {Component} from 'react';
import {Body, Container, Content} from "native-base";
import {Dimensions} from 'react-native'
import {BarChart, ContributionGraph, LineChart, PieChart, ProgressChart} from 'react-native-chart-kit'




const data={
  labels: ['January', 'February', 'March', 'April', 'May', 'June'],
  datasets: [{
    data: [
      Math.random() * 10,
      Math.random() * 10,
      Math.random() * 10,
      Math.random() * 10,
      Math.random() * 10,
      Math.random() * 10
    ]
  },
    {
      data: [
        Math.random() * 10,
        Math.random() * 10,
        Math.random() * 10,
        Math.random() * 10,
        Math.random() * 10,
        Math.random() * 10
      ]
    }

  ]
}

export class Charts extends Component {


  render() {

    return (<Container>
          <Content>
            <Body>
            <LineChart
                data={data}
                width={Dimensions.get('window').width-5}
                height={220}
                chartConfig={{
                  backgroundColor: '#cae229',
                  backgroundGradientFrom: '#2624fb',
                  backgroundGradientTo: '#ff5c81',
                  decimalPlaces: 1, // optional, defaults to 2dp
                  color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                  style: {
                    borderRadius: 16
                  }
                }}
                bezier
                style={{
                  marginVertical: 5,
                  borderRadius: 5
                }}
            />
            </Body>
          </Content>
        </Container>


    )
  }


}