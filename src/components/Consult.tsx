import React, {Component} from 'react';
import {Body, Button, Card, CardItem, Container, Icon, Left, List, ListItem, Right, Text,Content} from "native-base";
import {Image, StyleSheet} from 'react-native'
import _ from 'lodash'
import { Actions } from 'react-native-router-flux';

export class Consult extends Component {
	constructor(props) {
		super(props)
		console.log(props.navigation.getParam('user', null))
		this.state = {
			user: props.user
		}
	}

	showClient(c) {
		Actions.client({client:c})
	}

	getClientList() {
		return _.map(this.state.user.clients, c => {

			return (<ListItem key={c.id} onPress={this.showClient.bind(this, c)}>
				<Text>
					{c.firstName}
				</Text>
				<Body>
				<Text>
					{c.lastName}
				</Text>
				</Body>
				<Text>
					{c.companyName}
				</Text>
			</ListItem>)


		})

	}


	render() {

		return (<Container>
			<Content>


				<Card>
					<CardItem>
						<Left>
							<Body>
							<Text>{this.state.user.user.firstName} {this.state.user.user.lastName} {this.state.user.user.patronymic}</Text>
							</Body>
						</Left>

					</CardItem>

					<CardItem>
						<Left>
							<Body>
							<Text note>{this.state.user.user.companyName}</Text>
							</Body>
						</Left>

						<Right>
							<Button onPress={()=>Actions.chat()} transparent>
								<Icon active name="chatbubbles"/>
								<Text>1 сообщений</Text>
							</Button>
						</Right>

					</CardItem>
					<CardItem>
						<Left>
							<Button transparent>
								<Icon active name="call"/>
								<Text>{this.state.user.user.phoneNumber}</Text>
							</Button>
						</Left>
						<Right>
							<Button transparent>
								<Icon active name="mail"/>
								<Text>{this.state.user.user.email}</Text>
							</Button>
						</Right>
					</CardItem>
				</Card>
				<List>
					<ListItem>
						<Text>
						Клиенты: {this.state.user.clients.length}
						</Text>
					</ListItem>
					{this.getClientList()}


				</List>
			</Content>

		</Container>)


	}

}