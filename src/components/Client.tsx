import React, {Component} from 'react';
import {Body, Button, Card, CardItem, Container, Icon, Left, List, ListItem, Right, Text,Content} from "native-base";
import {Image, StyleSheet} from 'react-native'
import _ from 'lodash'
import { Actions } from 'react-native-router-flux';

export class Client extends Component {
	constructor(props) {
		super(props)
		console.log(props.user)
		this.state = {
			client: props.client
		}
	}

	showClient(c) {

	}

	getClientList() {
		return _.map(this.state.user.clients, c => {

			return (<ListItem key={c.id} onPress={this.showClient.bind(this, c)}>
				<Text>
					{c.firstName}
				</Text>
				<Body>
				<Text>
					{c.lastName}
				</Text>
				</Body>
				<Text>
					{c.companyName}
				</Text>
			</ListItem>)


		})

	}


	render() {

		return (<Container>
			<Content>


				<Card>
					<CardItem>
						<Left>
							<Body>
							<Text>{this.state.client.firstName} {this.state.client.lastName} {this.state.client.patronymic}</Text>
							</Body>
						</Left>

					</CardItem>

					<CardItem>
						<Left>
							<Body>
							<Text note>{this.state.client.companyName}</Text>
							</Body>
						</Left>

						<Right>
							<Button onPress={()=>Actions.chat()} transparent>
								<Icon active name="chatbubbles"/>
								<Text>1 сообщений</Text>
							</Button>
						</Right>

					</CardItem>
					<CardItem>
						<Left>
							<Button transparent>
								<Icon active name="call"/>
								<Text>{this.state.client.phoneNumber}</Text>
							</Button>
						</Left>
						<Right>
							<Button transparent>
								<Icon active name="mail"/>
								<Text>{this.state.client.email}</Text>
							</Button>
						</Right>
					</CardItem>
				</Card>
				<List>



					<ListItem icon onPress={()=>{Actions.costsPlan()}}>
						<Left>
							<Button style={{ backgroundColor: "#615945" }}>
								<Icon type="AntDesign" name="piechart" />
							</Button>
						</Left>
						<Body>
						<Text>Прогноз расходов</Text>
						</Body>
						<Right>
							<Icon type="SimpleLineIcons" name="arrow-right" />
						</Right>
					</ListItem>

					<ListItem icon onPress={()=>{Actions.salesPlan()}}>
						<Left>
							<Button style={{ backgroundColor: "#615945" }}>
								<Icon type="Foundation" name="burst-sale" />
							</Button>
						</Left>
						<Body>
						<Text>План продаж</Text>
						</Body>
						<Right>
							<Icon type="SimpleLineIcons" name="arrow-right" />
						</Right>
					</ListItem>

					<ListItem icon onPress={()=>{Actions.costsActual()}}>
						<Left>
							<Button style={{ backgroundColor: "#ffa054" }}>
								<Icon type="AntDesign" name="piechart" />
							</Button>
						</Left>
						<Body>
						<Text>Расходы</Text>
						</Body>
						<Right>
							<Icon type="SimpleLineIcons" name="arrow-right" />
						</Right>
					</ListItem>

					<ListItem icon onPress={()=>{Actions.salesActual()}}>
						<Left>
							<Button style={{ backgroundColor: "#ffa054" }}>
								<Icon type="Foundation" name="burst-sale" />
							</Button>
						</Left>
						<Body>
						<Text>Продажи</Text>
						</Body>
						<Right>
							<Icon type="SimpleLineIcons" name="arrow-right" />
						</Right>
					</ListItem>

					<ListItem icon onPress={()=>{Actions.products()}}>
						<Left>
							<Button style={{ backgroundColor: "#a69dff" }}>
								<Icon type="FontAwesome" name="product-hunt" />
							</Button>
						</Left>
						<Body>
						<Text>Продукты</Text>
						</Body>
						<Right>
							<Icon type="SimpleLineIcons" name="arrow-right" />
						</Right>
					</ListItem>


				</List>
			</Content>

		</Container>)


	}

}