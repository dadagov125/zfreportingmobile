import React, {Component} from 'react';
import {Button, Container, Content, Header, Icon, List, ListItem, Right, Separator, Text, View} from "native-base";
import {Image, StyleSheet} from 'react-native'
import _ from 'lodash'
import moment from 'moment'
import {Actions} from 'react-native-router-flux';

function onError(err) {
	console.log(err)
}


export class SaleList extends Component {

	static navigationOptions = {
		headerRight:(  <Button icon rounded transparent onPress={() => {
				Actions.saleEdit({type: 0, isNew: true, cost: null})
			}}>
				<Icon name='add'/>
			</Button>
		)
	};

	constructor(props) {
		super(props)
		this.state = {
			sales: []
		}

	}

	componentWillMount() {

		fetch('http://localhost:5000/api/sale')
			.then((res) => {
				res.json()
					.then(sales => {
						// console.log(sales)
						this.setState(state => {
							return Object.assign({}, state, {sales: sales})
						})
					}, onError)
			}, onError)

	}

	getSaleListItems() {
		return _.map(_.groupBy(this.state.items, 'date'), (sales, i) => {
			return (<View key={i}>
				<Separator bordered>
					<Text>{moment(i).format('MM YYYY')}</Text>
				</Separator>
				{this.getSaleList(sales)}
			</View>)


		});
	}

	getSaleList(sales) {
		return _.map(sales, s => {
			return (<View key={s.id}>
				<ListItem>
					<Text>{s.product.name}</Text>

					<Right style={styles.col}>
						<Text>цена {s.price}р</Text>
						<Text>количество {s.count}шт</Text>
					</Right>

				</ListItem>
			</View>)

		})
	}


	render() {
		return (
			<Container>
				<Content>
					<List>

						{this.getSaleListItems()}


					</List>

				</Content>
			</Container>


		)
	}

}

//
// <Picker
// 	mode="dropdown"
// 	placeholder="Тип продажи"
// 	// selectedValue={this.state.selected2}
// 	onValueChange={()=>{}}
// >
// 	<Picker.Item label="Прогноз продажи" value="0" />
// 	<Picker.Item label="Фактическая продажа" value="1" />
//
// </Picker>

const styles = StyleSheet.create({
	col: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'flex-start',
		alignItems: 'flex-end'
	}
});