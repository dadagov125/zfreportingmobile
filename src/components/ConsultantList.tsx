import React, {Component} from 'react';
import {Body, Container, Content, List, ListItem, Text} from "native-base";
import {Image, StyleSheet} from 'react-native'
import _ from 'lodash'
import { Actions } from 'react-native-router-flux';

function onError(err) {
	console.log(err)
}


export class ConsultantList extends Component {

	constructor(props) {
		super(props)

		console.log(props)
		this.state={
			users:[]
		}
	}

	showConsult(u){

		Actions.consult({user:u})
	}


	componentWillMount() {
		fetch('http://localhost:5000/api/user?role=Consult')
			.then((res) => {
				res.json()
					.then(users => {
						console.log(users)
						this.setState(state => {
							return Object.assign({}, state, {users: users})
						})
					}, onError)
			}, onError)
	}


	getUserListItems(){

	return	_.map(this.state.users,u=>{
			return (<ListItem key={u.user.id} onPress={this.showConsult.bind(this, u)}>
				<Text>
					{u.user.firstName}
				</Text>
				<Body>
				<Text>
					{u.user.lastName}
				</Text>
				</Body>
				<Text>
					клиентов: {u.clients.length}
				</Text>
			</ListItem>)
		})
	}

	render() {
		return (<Container>
				<Content>
					<List>
						{this.getUserListItems()}
					</List>
				</Content>
			</Container>


		)

	}


}