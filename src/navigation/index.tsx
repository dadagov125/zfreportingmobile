import React, {Component} from 'react'
import {Menu} from "../components/Menu";
import {Drawer, Lightbox, Modal, Overlay, Router, Scene, Stack} from "react-native-router-flux";
import {Home} from "../components/Home";
import {ConsultantList} from "../components/ConsultantList";
import {ClientList} from "../components/ClientList";
import {SaleList} from "../components/SaleList";
import {CostList} from "../components/CostList";
import {ProductList} from "../components/ProductList";
import {Charts} from "../components/Charts";
import Login from "../components/Login";
import {Consult} from "../components/Consult";
import {Client} from "../components/Client";
import {ProductEdit} from "../components/ProductEdit";
import {CostEdit} from "../components/CostEdit";
import {SaleEdit} from "../components/SaleEdit";
import {Chat} from "../components/Chat";
import {EntryLogo} from "../components/EntryLogo";

export default  () => (<Router>
    <Overlay hideNavBar key={'ovarlay'}>
      <Modal hideNavBar key={'key'}>
        <Lightbox  hideNavBar key={'lightbox'}>
          <Stack hideNavBar key={'root'}>
            <Drawer key={'drawer'}
                    onExit={() => console.log('Drawer closed')}
                    onEnter={() => console.log('Drawer opened')}
                    contentComponent={Menu}>

              <Scene key={'Главная'} component={Home}/>

              <Scene title={'Консультанты'} key={'consultants'} component={ConsultantList}/>

              <Scene title={'Клиенты'} key={'clients'} component={ClientList}/>

              <Scene title={'План Продаж'} key={'salesPlan'} component={SaleList}/>

              <Scene title={'Прогноз расходов'} key={'costsPlan'} component={CostList}/>

              <Scene title={'Продажи'} key={'salesActual'} component={SaleList}/>

              <Scene title={'Расходы'} key={'costsActual'} component={CostList}/>

              <Scene title={'Продукты'} key={'products'} component={ProductList}/>

              <Scene title={'Диаграмма'} key={'charts'} component={Charts}/>
            </Drawer>

            <Scene title={'Вход'} hideNavBar key={'login'} component={Login}/>

            <Scene title={'Консультант'} hideNavBar={false} key={'consult'} component={Consult}/>

            <Scene title={'Клиент'} hideNavBar={false} key={'client'} component={Client}/>

            <Scene title={'Продукт'} key={'productEdit'} component={ProductEdit}/>

            <Scene title={'Расход'} key={'costEdit'} component={CostEdit}/>

            <Scene title={'Продажа'} key={'saleEdit'} hideNavBar={false} component={SaleEdit}/>

            <Scene title={'Чат'} key={'chat'} hideNavBar={false} component={Chat}/>

            <Scene hideNavBar key={'entryLogo'} initial component={EntryLogo}/>

          </Stack>
        </Lightbox>
      </Modal>
    </Overlay>
  </Router>)


